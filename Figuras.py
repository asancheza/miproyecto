import math #importar libreria #bien
class Coordenada:
    def __init__(self,coorX,coorY):
        self.SetCoorX(coorX)
        self.SetCoorY(coorY)
    def SetCoorX(self,coorX):
        if type(coorX) is float or type(coorX) is int:
            self.__X=coorX
        else:
            print("Valor incorrecto. Ingresa un número.")
    def GetCoorX(self):
        return self.__X
    def SetCoorY(self,coorY):
        if type(coorY) is float or type(coorY) is int:
            self.__Y=coorY
        else:
            print("Valor incorrecto. Ingresa un número.")
    def GetCoorY(self):
        return self.__Y    

class Cuadrado:
    
    def __init__(self, lado,X,Y):
        self.__lado = lado
        self.__centro=Coordenada(X,Y)
    def calcular_superficie(self):
        print("LA Superficie del Cuadrado es: %d" %(self.__lado ** 2))
    def Setlado(self,lado):
        if lado<=0:
            print("Solo se puede Ingresar Lado Positivo y Diferente a Cero")            
        else:
            self.__lado=lado
    def Getlado(self):
        return "El Lado es: "+str(self.__lado)
    def setCentro(self,X,Y):
        self.__centro.SetCoorX(X)
        self.__centro.SetCoorY(Y)
    def getCentro(self):
        return "(" + str(self.__centro.GetCoorX())+ "," + str(self.__centro.GetCoorY()) + ")"

    
class Triangulo:
    def __init__(self,base,altura,X,Y):
        self.__base=base
        self.__altura=altura
        self.__centro=Coordenada(X,Y)
    def calcular_superficie(self):
        area=self.__base*self.__altura*0.5
        print("La Superficie del Triangulo es: %d" %area)
    def SetBase (self,base):
        if base<=0:
            print("La Base ha de ser Positiva y Diferente de Cerro")
        else:
            self.__base=base
    def GetBase(self):
        return "La Base es: "+str(self.__base)
    def SetAltura(self,altura):
        if altura<=0:
            print("La Altura ha de ser Positiva y Diferente de Cerro")            
        else:
            self.__altura=altura
    def GetAltura(self):
        return "La Altura es: "+str(self.__altura)
    def setCentro(self,X,Y):
        self.__centro.SetCoorX(X)
        self.__centro.SetCoorY(Y)
    def getCentro(self):
        return "(" + str(self.__centro.GetCoorX())+ "," + str(self.__centro.GetCoorY()) + ")"

class Circulo:
    def __init__(self, radio,X,Y):
        self.__radio = radio
        self.__centro=Coordenada(X,Y)
    def calcular_superficie(self):
        area = math.pi * self.__radio ** 2
        print("La superficie del circulo es: ", area)
    def SetRadio(self,radio):
        if radio<=0:
            print("El Radio ha de ser Positiva y Diferente de Cerro")
        else:
            self.__radio=radio
    def GetRadio(self):
        return "El Radio es: "+str(self.__radio)
    def setCentro(self,X,Y):
        self.__centro.SetCoorX(X)
        self.__centro.SetCoorY(Y)
    def getCentro(self):
        return "(" + str(self.__centro.GetCoorX())+ "," + str(self.__centro.GetCoorY()) + ")"
